/**
 * BankAccountService_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package bankaccount.hw4.mdw.fit.cvut.cz;

public interface BankAccountService_Service extends javax.xml.rpc.Service {
    public java.lang.String getBankAccountServicePortAddress();

    public bankaccount.hw4.mdw.fit.cvut.cz.BankAccountService_PortType getBankAccountServicePort() throws javax.xml.rpc.ServiceException;

    public bankaccount.hw4.mdw.fit.cvut.cz.BankAccountService_PortType getBankAccountServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
