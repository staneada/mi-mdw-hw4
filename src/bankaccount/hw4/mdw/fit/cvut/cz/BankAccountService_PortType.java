/**
 * BankAccountService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package bankaccount.hw4.mdw.fit.cvut.cz;

public interface BankAccountService_PortType extends java.rmi.Remote {
    public void addBalance(java.lang.String arg0, double arg1) throws java.rmi.RemoteException;
    public boolean checkBalance(java.lang.String arg0, double arg1) throws java.rmi.RemoteException;
    public boolean checkIfExists(java.lang.String arg0) throws java.rmi.RemoteException;
    public double getAccountBalance(java.lang.String arg0) throws java.rmi.RemoteException;
}
