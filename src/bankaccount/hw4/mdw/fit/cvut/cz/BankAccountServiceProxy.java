package bankaccount.hw4.mdw.fit.cvut.cz;

public class BankAccountServiceProxy implements bankaccount.hw4.mdw.fit.cvut.cz.BankAccountService_PortType {
  private String _endpoint = null;
  private bankaccount.hw4.mdw.fit.cvut.cz.BankAccountService_PortType bankAccountService_PortType = null;
  
  public BankAccountServiceProxy() {
    _initBankAccountServiceProxy();
  }
  
  public BankAccountServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initBankAccountServiceProxy();
  }
  
  private void _initBankAccountServiceProxy() {
    try {
      bankAccountService_PortType = (new bankaccount.hw4.mdw.fit.cvut.cz.BankAccountService_ServiceLocator()).getBankAccountServicePort();
      if (bankAccountService_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bankAccountService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bankAccountService_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bankAccountService_PortType != null)
      ((javax.xml.rpc.Stub)bankAccountService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public bankaccount.hw4.mdw.fit.cvut.cz.BankAccountService_PortType getBankAccountService_PortType() {
    if (bankAccountService_PortType == null)
      _initBankAccountServiceProxy();
    return bankAccountService_PortType;
  }
  
  public void addBalance(java.lang.String arg0, double arg1) throws java.rmi.RemoteException{
    if (bankAccountService_PortType == null)
      _initBankAccountServiceProxy();
    bankAccountService_PortType.addBalance(arg0, arg1);
  }
  
  public boolean checkBalance(java.lang.String arg0, double arg1) throws java.rmi.RemoteException{
    if (bankAccountService_PortType == null)
      _initBankAccountServiceProxy();
    return bankAccountService_PortType.checkBalance(arg0, arg1);
  }
  
  public boolean checkIfExists(java.lang.String arg0) throws java.rmi.RemoteException{
    if (bankAccountService_PortType == null)
      _initBankAccountServiceProxy();
    return bankAccountService_PortType.checkIfExists(arg0);
  }
  
  public double getAccountBalance(java.lang.String arg0) throws java.rmi.RemoteException{
    if (bankAccountService_PortType == null)
      _initBankAccountServiceProxy();
    return bankAccountService_PortType.getAccountBalance(arg0);
  }
  
  
}