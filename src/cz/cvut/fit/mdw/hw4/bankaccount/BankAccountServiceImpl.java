package cz.cvut.fit.mdw.hw4.bankaccount;

import java.util.*;

import javax.jws.*;

@WebService(portName = "BankAccountServicePort", serviceName = "BankAccountService", targetNamespace = "http://cz.cvut.fit.mdw.hw4.bankaccount/", endpointInterface = "cz.cvut.fit.mdw.hw4.bankaccount.BankAccountService")
public class BankAccountServiceImpl implements BankAccountService {
	
	static BankAccountDatabase db;
	
	private BankAccountDatabase getDatabase() {
		if(db == null) {
			db = new BankAccountDatabase();
			
			// Some test data
			db.add(new BankAccount("111222333/0111", 4100.0));
			db.add(new BankAccount("333444555/0111", -145.0));
		}
			
		return db;
	}
	
	public void addBalance(String accountNumber, double relativeValue) {
		BankAccount account = getDatabase().get(accountNumber);
		if(account == null) throw new IllegalArgumentException("Account does not exist");
	
		account.addBalance(relativeValue);
	}
	
	public boolean checkBalance(String accountNumber, double balance) {
		BankAccount account = getDatabase().get(accountNumber);
		if(account == null) throw new IllegalArgumentException("Account does not exist");
	
		return account.getBalance() >= balance;
	}
	
	public boolean checkIfExists(String accountNumber) {
		return getDatabase().get(accountNumber) != null;
	}
	
	// ------
	// For testing:
	
	public double getAccountBalance(String accountNumber) {
		
		BankAccount account = getDatabase().get(accountNumber);
		if(account == null) throw new IllegalArgumentException("Account does not exist");
	
		return account.getBalance();
	}
	
}
