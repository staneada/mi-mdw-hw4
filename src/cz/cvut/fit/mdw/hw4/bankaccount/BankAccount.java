package cz.cvut.fit.mdw.hw4.bankaccount;

import java.io.Serializable;

public class BankAccount implements Serializable {

	private static final long serialVersionUID = 8286393242028201686L;
	
	private String number;
	private double balance;
	
	public BankAccount(String number, double balance) {
		this.number = number;
		this.balance = balance;
	}
	
	public String getNumber() {
		return number;
	}
	
	public double getBalance() {
		return balance;
	}
	
	public void addBalance(double relativeValue) {
		 balance += relativeValue;
	}
	
}
