package cz.cvut.fit.mdw.hw4.bankaccount;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(name = "BankAccountService", targetNamespace = "http://cz.cvut.fit.mdw.hw4.bankaccount/")
public interface BankAccountService {
	
	@WebMethod(operationName = "addBalance")
	public void addBalance(String accountNumber, double relativeValue);
	
	@WebMethod(operationName = "checkBalance")
	public boolean checkBalance(String accountNumber, double balance);
	
	@WebMethod(operationName = "checkIfExists")
	public boolean checkIfExists(String accountNumber);
	
	// ------
	// For testing:
	
	@WebMethod(operationName = "getAccountBalance")
	public double getAccountBalance(String accountNumber);
	
}