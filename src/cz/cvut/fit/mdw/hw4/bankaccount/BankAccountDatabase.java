package cz.cvut.fit.mdw.hw4.bankaccount;

import java.util.*;

public class BankAccountDatabase {

	private HashMap<String, BankAccount> accounts;
	
	public BankAccountDatabase() {
		accounts = new HashMap<String, BankAccount>();
	}
	
	public void add(BankAccount account) {
		if(accounts.containsValue(account))
			return ;
		
		accounts.put(account.getNumber(), account);
	}
	
	public Collection<BankAccount> getAll() {
		return accounts.values();
	}
	
	public BankAccount get(String number) {
		if(!accounts.containsKey(number))
			return null;
		
		return accounts.get(number);
	}
	
}
