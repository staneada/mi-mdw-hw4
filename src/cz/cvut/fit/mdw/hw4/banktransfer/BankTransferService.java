package cz.cvut.fit.mdw.hw4.banktransfer;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(name = "BankTransferService", targetNamespace = "http://cz.cvut.fit.mdw.hw4.banktransfer/")
public interface BankTransferService {

	@WebMethod(operationName = "transfer")
	public void transfer(String from, String to, double amount);
}