package cz.cvut.fit.mdw.hw4.banktransfer;

import java.rmi.RemoteException;

import javax.jws.*;

import bankaccount.hw4.mdw.fit.cvut.cz.BankAccountServiceProxy;

@WebService(portName = "BankTransferServicePort", serviceName = "BankTransferServiceService", targetNamespace = "http://cz.cvut.fit.mdw.hw4.banktransfer/", endpointInterface = "cz.cvut.fit.mdw.hw4.banktransfer.BankTransferService")
public class BankTransferServiceImpl implements BankTransferService {

	public void transfer(String from, String to, double amount) {
		BankAccountServiceProxy proxy = new BankAccountServiceProxy();
		try {
			proxy.addBalance(from, 0 - amount);
			proxy.addBalance(to, amount);
			
		} catch (RemoteException e) {
			// TODO Throw something independent on RMI
		}
	}
}
